## [1.0.7](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.6...1.0.7) (2025-02-26)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.17 ([1a6e854](https://gitlab.com/appframework/egore-personal/osmgress/commit/1a6e8543155a9a3cd2d6912e30660ec8aa71f607))
* **deps:** update dependency com.google.code.gson:gson to v2.12.1 ([17fd596](https://gitlab.com/appframework/egore-personal/osmgress/commit/17fd59662333c0a6c2e000474977075876557292))

## [1.0.6](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.5...1.0.6) (2025-01-15)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.7.5 ([5ac4d74](https://gitlab.com/appframework/egore-personal/osmgress/commit/5ac4d744f5dbac0ac9b5335dc412669e84a6658a))

## [1.0.5](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.4...1.0.5) (2025-01-08)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.16 ([1931ecc](https://gitlab.com/appframework/egore-personal/osmgress/commit/1931eccdc471dff3c5474fd384db1cb5fa7edad7))

## [1.0.4](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.3...1.0.4) (2024-12-25)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.15 ([c466098](https://gitlab.com/appframework/egore-personal/osmgress/commit/c46609866537373f5c8dee23ea3b7a58ff593af7))

## [1.0.3](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.2...1.0.3) (2024-10-28)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.12 ([2b43968](https://gitlab.com/appframework/egore-personal/osmgress/commit/2b43968b4740dcdff60d31db605b37efbee64c13))

## [1.0.2](https://gitlab.com/appframework/egore-personal/osmgress/compare/1.0.1...1.0.2) (2024-10-15)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.11 ([2ef32c5](https://gitlab.com/appframework/egore-personal/osmgress/commit/2ef32c55041bdbdad5fe1c9d7ebb619c010c0e2a))

## [1.0.1](https://gitlab.com/appframework/egore-personal/osmgress/compare/v1.0.0...1.0.1) (2024-10-13)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.10 ([a85dde3](https://gitlab.com/appframework/egore-personal/osmgress/commit/a85dde3225728c41321c8528145318d0ef94a00b))

# 1.0.0 (2024-10-09)


### Bug Fixes

* **deps:** update dependency ch.qos.logback:logback-classic to v1.4.11 ([fa57d6c](https://gitlab.com/appframework/egore-personal/osmgress/commit/fa57d6ccaac0075ffac7dc3f97977cd77f570789))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.0 ([657e3b3](https://gitlab.com/appframework/egore-personal/osmgress/commit/657e3b3c6e8dfe6b02ae1b2fdfb8dd12e13bb70f))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.3 ([512ba50](https://gitlab.com/appframework/egore-personal/osmgress/commit/512ba50da3c6527cd8a145b688555d2240004669))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.4 ([45718ec](https://gitlab.com/appframework/egore-personal/osmgress/commit/45718ec68d034405a04d8cacb999a9c94b21ab88))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.5 ([4fafa92](https://gitlab.com/appframework/egore-personal/osmgress/commit/4fafa922e43719159ce9052f0e5adf453fb4e454))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.6 ([729debe](https://gitlab.com/appframework/egore-personal/osmgress/commit/729debe523de9440aec4cf37e170c5d54353a29b))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.7 ([e11029d](https://gitlab.com/appframework/egore-personal/osmgress/commit/e11029d1505ba8f13cfa9d139a0cc0bd936a6b3d))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.8 ([f9d4669](https://gitlab.com/appframework/egore-personal/osmgress/commit/f9d4669f996fa69271a203f92d8c6be4e721a949))
* **deps:** update dependency ch.qos.logback:logback-classic to v1.5.9 ([62022f6](https://gitlab.com/appframework/egore-personal/osmgress/commit/62022f6e8c63d3a7d3e1dfb4b68744912dc1932c))
* **deps:** update dependency com.google.code.gson:gson to v2.11.0 ([87e1049](https://gitlab.com/appframework/egore-personal/osmgress/commit/87e1049561ad61adb3855c71e27d8fea453708ad))
* **deps:** update dependency org.postgresql:postgresql to v42.7.4 ([b62850c](https://gitlab.com/appframework/egore-personal/osmgress/commit/b62850cb7249ad49384b1778d8baee431aa74d32))
